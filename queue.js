let collection = [];

// Write the queue functions below.

function print() {
	
	console.log(collection)
	return collection

}

function enqueue(element) {
	collection.push(element)
	return collection

}

function dequeue() {
	collection.shift()
	return collection

}

function front() {
	return collection[0]
	
}

function size() {
	return collection.length


}

function isEmpty() {
	if(collection.length) {
		return false
	} else {
		return true
	}
	return collection
}

module.exports = {
	collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};